﻿using FlightManagementSystem.CustomExceptions;
using FlightManagementSystem.Models;
using FlightManagementSystem.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class FlightManagementController : ControllerBase
    {

        private IFlightManagementService _flightManagementService;

        public FlightManagementController(IFlightManagementService flightManagementService)
        {

            _flightManagementService = flightManagementService;
        }

        [HttpPost("addpassenger")]
        public async Task<ActionResult> AddPassenger(Passenger passenger)
        {

            try
            {

                await _flightManagementService.AddPassenger(passenger);

                return Created("AddPassenger", new { success = "Inserted successfully" });
            }
            catch (Exception ex)
            {

                return BadRequest(new { falied = ex.Message });
            }
        }

        [HttpGet("flights")]
        public async Task<ActionResult<List<Flight>>> GetFlights()
        {

            try
            {

                var flightDetails = await _flightManagementService.GetFlights();

                return Ok(flightDetails);
            }
            catch (Exception ex)
            {

                return BadRequest(new { falied = ex.Message });
            }
        }

        [HttpGet("passengersonflight")]
        public async Task<ActionResult<List<Flight>>> GetPassengersDetailsFromFlightName([FromQuery] string flightName)
        {

            try
            {

                var flightDetails = await _flightManagementService.GetPassengersDetailsFromFlightName(flightName);

                return Ok(new { data = flightDetails });
            }
            catch (FlightNameNotFoundException ex)
            {

                return BadRequest(new { falied = ex.Message });
            }
            catch (PassengersDetailsNotFoundException ex)
            {

                return BadRequest(new { falied = ex.Message });
            }
            catch (Exception ex)
            {

                return BadRequest(new { falied = ex.Message });
            }
        }
    }
}
