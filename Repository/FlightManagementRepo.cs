﻿using FlightManagementSystem.CustomExceptions;
using FlightManagementSystem.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.Repository
{
    public class FlightManagementRepo : IFlightManagementRepo
    {

        private FlightManagementDbContext _flightManagementDbContext;

        public FlightManagementRepo(FlightManagementDbContext flightManagementDbContext)
        {

            _flightManagementDbContext = flightManagementDbContext;
        }

        public async Task<int> AddPassenger(Passenger passenger)
        {

            try
            {

                _flightManagementDbContext.Passengers.Add(passenger);
                return await _flightManagementDbContext.SaveChangesAsync();

                //return passenger.PassengerId;
            }
            catch (SqlException ex)
            {

                throw new CustomSqlException("Can't connect to database", ex);
            }
        }

        public async Task<List<Flight>> GetFlights()
        {

            try
            {

                var details = await _flightManagementDbContext.Flights.ToListAsync();

                return details;
            }
            catch (SqlException ex)
            {

                throw new CustomSqlException("Can't connect to database", ex);
            }
        }

        public async Task<List<Passenger>> GetPassengersDetailsFromFlightName(string flightName)
        {

            try
            {

                var flightId = _flightManagementDbContext.Flights.Where(f => f.FlightName == flightName).Select(p => p.FlightId).SingleOrDefault();

                if (flightId == 0)
                {

                    throw new FlightNameNotFoundException("Invalid flight name!!!");
                }

                var passengerDetails = await _flightManagementDbContext.Passengers.Where(p => p.FlightId == flightId).ToListAsync();

                if (passengerDetails.Count == 0)
                {

                    throw new PassengersDetailsNotFoundException("No details found for passenger in flight!!!");
                }

                return passengerDetails;

            }
            catch (SqlException ex)
            {

                throw new CustomSqlException("Can't connect to database", ex);
            }
        }
    }
}
