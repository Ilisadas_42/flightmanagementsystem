﻿using FlightManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.Repository
{
    public interface IFlightManagementRepo
    {

        Task<int> AddPassenger(Passenger passenger);
        Task<List<Flight>> GetFlights();
        Task<List<Passenger>> GetPassengersDetailsFromFlightName(string flightName);
    }
}
