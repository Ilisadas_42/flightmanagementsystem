﻿using FlightManagementSystem.CustomExceptions;
using FlightManagementSystem.Models;
using FlightManagementSystem.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.Services
{
    public class FlightManagementService : IFlightManagementService
    {

        //private readonly log4net.ILog _log4net = log4net.LogManager.GetLogger("Demolog");

        private readonly IFlightManagementRepo _flightManagementRepo;

        private readonly ILogger _logger;

        public FlightManagementService(IFlightManagementRepo flightManagementRepo, ILogger<FlightManagementService> logger)
        {

            _flightManagementRepo = flightManagementRepo;

            _logger = logger;
        }

        //// using logger factory
        /*public FlightManagementService(IFlightManagementRepo flightManagementRepo, ILoggerFactory factory)
        {

            _flightManagementRepo = flightManagementRepo;

            _logger = factory.CreateLogger("DemoLogger");
        }*/

        public async Task<int> AddPassenger(Passenger passenger)
        {

            if (passenger.FlightId == 0)
            {

                throw new MissingFlightDetailsException("Passenger should contain flight details");
            }

            try
            {

                return await _flightManagementRepo.AddPassenger(passenger);
            }
            catch (Exception ex)
            {

                Exception exception = (ex.InnerException != null) ? ex.InnerException : ex;

                _logger.LogError(exception, "Exception in AddPassenger");

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Flight>> GetFlights()
        {

            try
            {

                return await _flightManagementRepo.GetFlights();
            }
            catch (Exception ex)
            {
                Exception exception = (ex.InnerException != null) ? ex.InnerException : ex;
                _logger.LogError(exception, "Exception in GetFlights");

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<Passenger>> GetPassengersDetailsFromFlightName(string flightName)
        {

            try
            {

                return await _flightManagementRepo.GetPassengersDetailsFromFlightName(flightName);
            }
            catch (Exception ex)
            {

                //Log Providers
                /*Console
                  Debug
                  EventSource
                  EventLog: Windows only*/

                //Logging levels
                /*_logger.LogTrace("");
                _logger.LogDebug("");
                _logger.LogTrace("");
                _logger.LogInformation("");
                _logger.LogWarning("");
                _logger.LogError("");
                _logger.LogCritical("");*/

                Exception exception = (ex.InnerException != null) ? ex.InnerException : ex;

                _logger.LogTrace("sffse");

               // _logger.LogError(1, exception, "There was a exception in GetPassengersDetailsFromFlightName {Time}", DateTime.Now);

                _logger.LogError(ex.Message);
                //_log4net.Error(ex.Message);

                throw new Exception(ex.Message);
            }
        }
    }
}
