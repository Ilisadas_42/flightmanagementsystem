﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.CustomExceptions
{
    public class PassengersDetailsNotFoundException : Exception
    {

        public PassengersDetailsNotFoundException(string message) : base(message)
        {

        }
    }
}
