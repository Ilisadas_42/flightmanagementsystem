﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.CustomExceptions
{
    public class FlightNameNotFoundException : Exception
    {

        public FlightNameNotFoundException(string message) : base(message)
        {

        }
    }
}
