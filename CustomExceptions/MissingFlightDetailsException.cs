﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.CustomExceptions
{
    public class MissingFlightDetailsException : Exception
    {

        public MissingFlightDetailsException(string message) : base(message)
        {

        }
    }
}
