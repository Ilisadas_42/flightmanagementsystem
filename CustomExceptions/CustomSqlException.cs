﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightManagementSystem.CustomExceptions
{
    public class CustomSqlException : Exception
    {

        public CustomSqlException(string message, Exception ex) : base(message, ex)
        {

        }
    }
}
