﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FlightManagementSystem.Models
{
    [Table("Passenger")]
    public partial class Passenger
    {
        [Key]
        public int PassengerId { get; set; }
        [StringLength(50)]
        public string PassengerName { get; set; }
        public int? PassengerAge { get; set; }
        public int? FlightId { get; set; }

        [ForeignKey(nameof(FlightId))]
        [InverseProperty("Passengers")]
        public virtual Flight Flight { get; set; }
    }
}
